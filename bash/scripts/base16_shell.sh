#!/usr/bin/env bash

eval sh "${HOME}/.base16_theme"

if [ -d "${HOME}/.local/share/base16-manager/chriskempson/base16-shell" ]; then
  BASE16_SHELL="${HOME}/.local/share/base16-manager/chriskempson/base16-shell/"
elif [ -d "${HOME}/.base16-manager/chriskempson/base16-shell" ]; then
  BASE16_SHELL="${HOME}/.base16-manager/chriskempson/base16-shell/"
fi

[ -n "$PS1" ] && [ -s "${BASE16_SHELL}/profile_helper.sh" ] && \
  eval "$("${BASE16_SHELL}/profile_helper.sh")"

