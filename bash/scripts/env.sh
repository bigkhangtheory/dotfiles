
export TERM='xterm-termite'
export COLORTERM='truecolor'
export EDITOR='vim'
export VISUAL='vim'
export MANPAGER='vimpager'
export BROWSER='chromium'
export CLICOLOR=1
export PATH=${HOME}/.local/bin:${PATH}
export npm_config_prefix=${HOME}/.node_modules
export PATH=${HOME}/.node_modules/bin:${PATH}
