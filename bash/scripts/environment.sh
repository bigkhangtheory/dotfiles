#!/bin/sh

# terminal definitions
export TERM='xterm-256color'
export COLORTERM='truecolor'
export CLICOLOR=1

# python definitions
export PYTHON=python3
export PYTHONPYCACHEPREFIX="${HOME}/.cache/cpython/"

# editor defintions
if [ -x "$(which nvim)" ]; then
  export EDITOR='nvim'
  export VISUAL="nvim"
else
  export EDITOR='vim'
  export VISUAL='vim'
fi

# man pager definitions
if [ -x "$(which nvimpager)" ]; then
  export MANPAGER='nvimpager'
elif [ -x "$(which vimpager)" ]; then
  export MANPAGER='vimpager'
fi

# ccache path
if [ -x "$(which ccache)" ]; then
  export PATH="/usr/lib/ccache/bin:${PATH}"
fi

# rust cargo packages
if [ -x "$(which cargo)" ]; then
  export PATH="${HOME}/.cargo/bin:${PATH}"
fi

# node.js environment
if [ -x "$(which npm)" ]; then
  export npm_config_prefix="${HOME}/.node_modules"
  export PATH="${PATH}:${npm_config_prefix}/bin"
fi
