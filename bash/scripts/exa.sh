#!/bin/sh

# header row: size numbers: size unit: user: notuser: group: notgroup: timestamp: symlink path: puntuation: inode:
export EXA_COLORS="hd=38;5;20:sn=38;5;17:sb=38;5;17:uu=1;30:un=31:gu=1;30:gn=31:da=1;30:lp=32:xx=38;5;20:in=38;5;17"

# limits the grid-details view so it's only activated
# when at least the given number of rows of output
# would be generated
export EXA_GRID_ROWS=8

# overrides the width of the terminal when using a gridview
# export COLUMNS=

# time formating when used in long view's time column
export TIME_STYLE="default"

