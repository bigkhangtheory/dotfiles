# GCC variables

CARCH="x86_64"
CHOST="x86_64-pc-linux-gnu"

# colored GCC warnings and errors
export GCC_COLORS="error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01"

export CPPFLAGS="-D_FORTIFY_SOURCE=2"
export CFLAGS="-march=skylake -O3 -pipe -fno-plt"
export CXXFLAGS="${CFLAGS}"
export LDFLAGS="-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now"
export RUSTFLAGS="-C opt-level=3"

export DEBUG_CFLAGS="-g -fvar-tracking-assignments"
export DEBUG_CXXFLAGS="-g -fvar-tracking-assignments"
#export DEBUG_RUSTFLAGS="-C debuginfo=2"
