#!/usr/bin/env bash

if [ -x $(which powerline-daemon) ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTUNUATION=1
  POWERLINE_BASH_SELECT=1
  . ${HOME}/.local/lib/python3.8/site-packages/powerline/bindings/bash/powerline.sh
fi
