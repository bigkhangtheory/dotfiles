/**
 * smooth scroll settings
*/
user_pref("general.smoothScroll.lines.durationMaxMS", 125);
user_pref("general.smoothScroll.lines.durationMinMS", 125);
user_pref("general.smoothScroll.mouseWheel.durationMaxMS", 200);
user_pref("general.smoothScroll.mouseWheel.durationMinMS", 100);
user_pref("general.smoothScroll.msdPhysics.enabled", true);
user_pref("general.smoothScroll.other.durationMaxMS", 125);
user_pref("general.smoothScroll.other.durationMinMS", 125);
user_pref("general.smoothScroll.pages.durationMaxMS", 125);
user_pref("general.smoothScroll.pages.durationMinMS", 125);
/**
 * mouse wheel settings
 */
user_pref("mousewheel.acceleration.start", -1);
user_pref("mousewheel.acceleration.factor", 20);
user_pref("mousewheel.default.delta_multiplier_y", 200);
user_pref("mousewheel.min_line_scroll_amount", 40);
user_pref("mousewheel.system_scroll_override_on_root_content.enabled", true);
user_pref("mousewheel.system_scroll_override_on_root_content.horizontal.factor", 175);
user_pref("mousewheel.system_scroll_override_on_root_content.vertical.factor", 175);
user_pref("toolkit.scrollbox.horizontalScrollDistance", 6);
user_pref("toolkit.scrollbox.verticalScrollDistance", 2);
/**
 * performance tweaks
 */
user_pref("dom.ipc.processCount", 8);
user_pref("browser.preferences.defaultPerformanceSettings.enabled", false);
/**
 * disk cache settings
 */
user_pref("browser.cache.disk.enable", false);
user_pref("browser.cache.memory.enable", true);
user_pref("browser.cache.memory.capacity", -1);
user_pref("browser.sessionstore.resume_from_crash", false);
/**
 * DPI settings
 */
user_pref("layout.css.dpi", 0);
