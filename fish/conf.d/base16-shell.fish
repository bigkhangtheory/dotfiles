#!/usr/bin/env fish

if status --is-interactive
    source "$HOME/.config/base16-shell/profile_helper.fish"
end
