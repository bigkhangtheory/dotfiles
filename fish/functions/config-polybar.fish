#!/usr/bin/env fish
#
function config-polybar
    nvim ~/.config/polybar/config
end
