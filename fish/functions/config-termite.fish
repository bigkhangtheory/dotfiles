#!/usr/bin/env fish
#
function config-termite
    nvim ~/.config/termite/config
end
