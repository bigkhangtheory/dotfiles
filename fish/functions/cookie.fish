#!/usr/bin/env fish
function cookie
    clear
    set_color $fish_color_autosuggestion
    fortune -s
    set_color normal
end
