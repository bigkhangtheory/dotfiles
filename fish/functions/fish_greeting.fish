#!/usr/bin/env fish
#
function fish_greeting
    set_color $fish_color_autosuggestion
    fortune -s
    set_color normal
end
