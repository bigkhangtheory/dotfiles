#!/usr/bin/env fish
#
function globalprotect
    sudo openconnect --protocol=gp gp.cs.odu.edu --dump -vvv
end
