#!/usr/bin/env fish
#
function gtop
    /sbin/python /sbin/glances --percpu --time 3 --diskio-iops --process-short-name
end
