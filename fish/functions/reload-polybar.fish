#!/usr/bin/env fish
#
function reload-polybar
    ~/.config/polybar/scripts/launch.sh
end
