#!/usr/bin/env fish
#
function run-journal
    journalctl --all --follow --lines=16 --output=short-full --no-hostname | ccze -p syslog
end
