#!/usr/bin/env fish
#
function tmux-consoles
    tmuxinator start consoles
end
