#!/usr/bin/env fish
#
function tmux-stats
    tmuxinator start stats
end
