#!/bin/sh

TOKEN="d73701d6d628c5b9429c3b5046031952c9d5a781"
CITY="usa/virginia/nasa-langley-research-center"

API="https://api.waqi.info/feed"

if [ -n "$CITY" ]; then
    aqi=$(curl -sf "$API/$CITY/?token=$TOKEN")
else
    location=$(curl -sf https://location.services.mozilla.com/v1/geolocate?key=geoclue)

    if [ -n "$location" ]; then
        location_lat="$(echo "$location" | jq '.location.lat')"
        location_lon="$(echo "$location" | jq '.location.lng')"

        aqi=$(curl -sf "$API/geo:$location_lat;$location_lon/?token=$TOKEN")
    fi
fi

if [ -n "$aqi" ]; then
    if [ "$(echo "$aqi" | jq -r '.status')" = "ok" ]; then
        aqi=$(echo "$aqi" | jq '.data.aqi')

        if [ "$aqi" -lt 50 ]; then
            echo "%{F#26a98b}%{F-}  air index: $aqi "
        elif [ "$aqi" -lt 100 ]; then
            echo "%{F#edb54b}%{F-}  air index: $aqi "
        elif [ "$aqi" -lt 150 ]; then
            echo "%{F#d26939}%{F-}  air index: $aqi "
        elif [ "$aqi" -lt 200 ]; then
            echo "%{F#c33027}%{F-}  air index: $aqi "
        elif [ "$aqi" -lt 300 ]; then
            echo "%{F#4e5165}%{F-}  air index: $aqi "
        else
            echo "%{F#888ba5}%{F-}  air index: $aqi "
        fi
    else
        echo "$aqi" | jq -r '.data'
    fi
fi
