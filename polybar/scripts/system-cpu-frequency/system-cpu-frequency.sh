#!/bin/sh

c=0;t=0

cpu=$(awk '/MHz/ {print $4}' < /proc/cpuinfo | (while read -r i
do
    t=$( echo "$t + $i" | bc )
    c=$((c+1))
done
echo "scale=2; $t / $c / 1000 " | bc | awk '{print $1" GHz"}'))

gpu=$(rocm-smi --showgpuclocks \
  | grep "GPU" \
  | awk '{ print $7 }' \
  | sed s/\(//g \
  | sed s/\)//g \
  | sed s/Mhz//g \
  | (while read -r i
do
    t=$( echo "$t + $i" | bc )
    c=$((c+1))
done
echo "scale=0; $t / $c / 1 " | bc | awk '{print $1" MHz"}'))

echo "${cpu} ${gpu}"
