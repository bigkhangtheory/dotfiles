#!/bin/sh

speed1=$(sensors | grep fan1 | cut -d " " -f 9)
speed2=$(sensors | grep fan2 | cut -d " " -f 9)


if [ "$speed1" = "" ]; then
   speed1="N/A"
else
  speed1+="R"
fi

if [ "$speed2" = "" ]; then
   speed2="N/A"
else
  speed2+="R"
fi

echo "${speed1} ${speed2}"
