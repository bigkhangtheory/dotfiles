#!/bin/sh

gpu=$(rocm-smi --showtemp | grep "GPU" | awk '{ print $7 }' | sed s/\\..//g)

echo "${gpu}°"
