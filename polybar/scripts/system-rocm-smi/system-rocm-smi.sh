#!/bin/sh

#use=$(rocm-smi --showuse | grep "GPU" | awk '{ print $6 }')
temp=$(rocm-smi --showtemp | grep "GPU" | awk '{ print $7 }')
clock=$(rocm-smi --showgpuclocks | grep "GPU" | awk '{ print $7 }' | sed s/\(//g | sed s/\)//g)
#fan=$(sensors | grep fan2 | cut -d " " -f 9)

#if [ "$use" == "" ]; then
#  use="N/A"
#fi

if [ "$temp" == "" ]; then
  temp="N/A"
fi

if [ "$clock" == "" ]; then
  clock="N/A"
fi

echo " ${clock} ${temp}°"
