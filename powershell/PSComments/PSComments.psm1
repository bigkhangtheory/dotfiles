function Clear-CommentsFromFile {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String]
        $FilePath
    )
    begin {
        $f = $FilePath
    }
    process {
        Get-Content $f | Where-Object { $_ -notmatch '^\s*#' } | ForEach-Object { $_ -replace '(^.*?)\s*?[^``]#.*', '$1' } | Out-File $f+".~" -en utf8; Move-Item -fo $f+".~" $f
    }
 
 
 
 
}
