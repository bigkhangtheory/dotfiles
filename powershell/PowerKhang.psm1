#requires -Version 2 -Modules posh-git

function Write-Theme {
    param(
        [bool]
        $lastCommandFailed,
        [string]
        $with
    )
    # check the last command state and indicate if failed
    If ($lastCommandFailed) {
        $prompt += Write-Prompt -Object "$($sl.PromptSymbols.FailedCommandSymbol) " -ForegroundColor $sl.Colors.CommandFailedIconForegroundColor
    }

    <#
        .SECTION USER
    #>
    #$prompt = Write-Prompt @userSymbol
    if (Test-NotDefaultUser($sl.CurrentUser)) {
        $prompt += Write-Prompt @userSegmentLeft
        $prompt += Write-Prompt @userSegment
        $prompt += Write-Prompt @userSegmentRight
    }

    #$prompt += Write-Prompt -Object " on " -ForegroundColor $sl.Colors.Gray

    <#
        .SECTION HOST
    #>
    #$prompt += Write-Prompt @hostSymbol
    $prompt += Write-Prompt @hostSegmentLeft
    $prompt += Write-Prompt @hostSegment
    $prompt += Write-Prompt @hostSegmentRight

    #$prompt += Write-Prompt -Object " in " -ForegroundColor $sl.Colors.Gray


    <#
        .SECTION DIRECTORY
    #>
    $prompt += Write-Prompt @dirSegmentLeft
    $dir    = [String]::Format(" {0} ", (Get-ShortPath -dir $pwd))
    $prompt += Write-Prompt @dirSegment -Object $dir
    $prompt += Write-Prompt @dirSegmentRight


    <#
        .SECTION GIT STATUS
    #>
    $status = Get-VCSStatus
    if ($status) {
        $themeInfo = Get-VcsInfo -status ($status)
        $lastColor = $themeInfo.BackgroundColor
        #$prompt += Write-Prompt -Object 'on git:' -ForegroundColor $foregroundColor
        $prompt += Write-Prompt @gitSegmentLeft -BackgroundColor $lastColor
        $prompt += Write-Prompt -Object "$($themeInfo.VcInfo) " -BackgroundColor $lastColor -ForegroundColor $sl.Colors.Black
        #$prompt += Write-Prompt @gitSegmentRight -ForegroundColor $lastColor
        <#
            .SECTION WITH
        #>
        if ($with) {
            $prompt += Write-Prompt -Object $sl.PromptSymbols.SegmentForwardSymbol -ForegroundColor $lastColor -BackgroundColor $sl.Colors.WithBg
            $prompt += Write-Prompt -Object " $($with.ToUpper()) " -BackgroundColor $sl.Colors.WithBg -ForegroundColor $sl.Colors.WithFg
            $lastColor = $sl.Colors.WithBg
    }
    $prompt += Write-Prompt -Object $sl.PromptSymbols.PowerlineRightHard -ForegroundColor $lastColor
    }
    <#
        .SECTION TIME
    #>
    $timeStamp = Get-Date -Format "hh:mm tt"
    if (($host.UI.RawUI.CursorPosition.X + $timeStamp.Length) -lt $host.UI.RawUI.WindowSize.Width) {
        $prompt += Set-CursorForRightBlockWrite -textLength ($timeStamp.Length)
        $prompt += Write-Prompt "$timeStamp" -ForegroundColor $sl.Colors.Gray
    }

    # new line
    $prompt += Set-Newline
    if (Test-Administrator) {
        $prompt += Write-Prompt @psAdmin
    }
    else{
        $prompt += Write-Prompt @psPrompt
    }
}

$sl = $global:ThemeSettings # local settings

<##region
    .SECTION COLORS
#>
$sl.Colors.Black        = [ConsoleColor]::Black
$sl.Colors.Red          = [ConsoleColor]::DarkRed
$sl.Colors.Green        = [ConsoleColor]::DarkGreen
$sl.Colors.Yellow       = [ConsoleColor]::DarkYellow
$sl.Colors.Blue         = [ConsoleColor]::DarkBlue
$sl.Colors.Magenta      = [ConsoleColor]::DarkMagenta
$sl.Colors.Cyan         = [ConsoleColor]::DarkCyan
$sl.Colors.Gray         = [ConsoleColor]::DarkGray
$sl.Colors.BrRed        = [ConsoleColor]::Red
$sl.Colors.BrGreen      = [ConsoleColor]::Green
$sl.Colors.BrYellow     = [ConsoleColor]::Yellow
$sl.Colors.BrBlue       = [ConsoleColor]::Blue
$sl.Colors.BrMagenta    = [ConsoleColor]::Magenta
$sl.Colors.BrCyan       = [ConsoleColor]::Cyan

$sl.Colors.User             = $sl.Colors.Magenta
$sl.Colors.Host             = $sl.Colors.Cyan
$sl.Colors.Dir              = $sl.Colors.Blue
$sl.Colors.GitDefaultColor  = $sl.Colors.Green
$sl.Colors.WithBG           = $sl.Colors.Red
$sl.Colors.WithFG           = $sl.Colors.Black
#endregion

<##region
    .SECTION DEFAULT SYMBOLS
#>
$sl.PromptSymbols.Status = @{
    FaCheck             = ''
    FaCheckSquare       = ''
    FaCheckSquareO      = ''
    FaEyeSlash          = ''
    FaExclamation       = ''
    FaLevelDown         = ''
    FaLevelUp           = ''
    FaQuestion          = ''
    MdiAlertO       = ''
    MdiCanceled     = 'ﰸ'
    MdiCommentAlert     = ''
    MdiCommentAlertO    = ''
    MdiCommentCheck     = ''
    MdiCommentCheckO    = ''
    MdiCommentProcess   = ''
    MdiCommentProcessO  = ''
    MdiCloudCheck       = ''
    MdiCloudDownload    = ''
    MdiCloudSync        = 'מּ'
    MdiCloudUpload      = ''
    MdiEmoticonDead     = 'ﮙ'
    MdiEyeOff           = ''
    MdiEyeOffO          = '﯏'
    OctCloudDownload    = ''
    OctCloudUpload      = ''
    OctSync             = ''
   OctThumbsUp     = ''
    OctThumbsDown   = ''
}
$sl.PromptSymbols.FailedCommandSymbol   = [String]::Format("{0} ", $sl.PromptSymbols.Status.MdiEmoticonDead)

$sl.PromptSymbols.Powerline = @{
    PlLeftHardDivider   = ''
    PlLeftSoftDivider   = ''
    PleLowerLeftTri     = ''
    PleBackSlash        = ''
    PleRightCircleThick = ''
    PleRightCircleThin  = ''
    PleTrapezoid        = ''
}

$sl.PromptSymbols.SegmentForwardSymbol  = [String]::Format("{0}", $sl.PromptSymbols.Powerline.PlLeftHardDivider)
$sl.PromptSymbols.PowerlineRightHard    = $sl.PromptSymbols.SegmentForwardSymbol
$sl.PromptSymbols.PowerlineRightSoft    = [String]::Format("{0}", $sl.PromptSymbols.Powerline.PlLeftSoftDivider)

$sl.PromptSymbols.PowerlineLeftHard     = [String]::Format("{0}", $sl.PromptSymbols.Powerline.PleTrapezoid)

$sl.PromptSymbols.PathSeparator = [String]::Format(" {0} ", $sl.PromptSymbols.PowerlineRightSoft)

$sl.PromptSymbols.Home = @{
    FaHome        = ''
    MdiHome       = ''
    OctHome       = ''
    SetiHome      = ''
    MdiTilda      = 'ﰣ'
}
$sl.PromptSymbols.HomeSymbol    = [String]::Format("{0}", $sl.PromptSymbols.Home.SetiHome)

$sl.PromptSymbols.Root = @{
    FaHdd           = ''
    MdiDesktop      = 'ﲾ'
    MdiDesktopTower = ''
    FaGlobe         = ''
}
$sl.PromptSymbols.RootSymbol    = [String]::Format(" {0} ", $sl.PromptSymbols.Root.MdiDesktopTower)

$sl.PromptSymbols.Ellipsis = @{
    FaEllipsis    = ''
    OctEllipsis   = ''
    MdiDots       = ''
}
$sl.PromptSymbols.TruncatedFolderSymbol = [String]::Format("{0}", $sl.PromptSymbols.Ellipsis.MdiDots)
<#
    .SECTION GIT SYMBOLS
#>
$sl.PromptSymbols.Git = @{
    DevGit          = ''
    FaGit           = ''
    FaGitSquare     = ''
    DevGitBranch    = ''
    DevGitCommit    = ''
    MdiGitBranch    = 'שׂ'
    OctGitBranch    = ''
    OctGitCommit    = ''
    FaCode          = ''
    FaCodeFile      = ''
    MdiCode         = ''
    MdiCodeCheck    = 'ﮒ'
    MdiCodeBraces   = ''
    OctCode         = ''
}
$sl.GitSymbols.BranchSymbol                     = [String]::Format("{0}", $sl.PromptSymbols.Git.OctGitBranch)
$sl.GitSymbols.BranchBehindStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaLevelDown)
$sl.GitSymbols.BranchAheadStatusSymbol          = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaLevelUp)
$sl.GitSymbols.BranchUntrackedSymbol            = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaEyeSlash)
$sl.GitSymbols.BranchIdenticalStatusToSymbol    = [String]::Format("{0} ", $sl.PromptSymbols.Status.OctSync)
$sl.GitSymbols.LocalDefaultStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaCheck)
$sl.GitSymbols.LocalWorkingStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaQuestion)
$sl.GitSymbols.LocalStagedStatusSymbol          = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaExclamation)
$sl.GitSymbols.BeforeWorkingSymbol              = [String]::Format("{0} ", $sl.PromptSymbols.Git.MdiCode)
$sl.GitSymbols.BeforeIndexSymbol                = [String]::Format("{0} ", $sl.PromptSymbols.Git.MdiCodeCheck)

$gitSegmentLeft = @{
    Object          = [String]::Format("{0} ", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Black
}

<#$gitSegmentRight = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
}#>

<## region
    .SECTION USER
#>
$sl.PromptSymbols.User = @{
    FaUser            = ''
    FaUserO           = ''
    FaUserCircle      = ''
    FaUserCircleO     = ''
    FaIdCard          = ''
    FaAddressCard     = ''
    MdiAccount          = ''
    MdiAccountCircle  = ''
    MdiAccountBox     = ''
    MdiAccountCard    = '𣏕'
    OctPerson           = ''
}
<#$userSymbol = @{
    Object          = [String]::Format("{0}", "")
    ForegroundColor = $sl.Colors.User
}#>
$userSegmentLeft = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.User
}
$userSegment = @{
    Object          = [String]::Format(" {0} {1} ", $sl.PromptSymbols.User.MdiAccount, $sl.CurrentUser)
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.User
}
$userSegmentRight = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.User
}
# endregion
<##region
    .SECTION HOST
#>
$sl.PromptSymbols.Host = @{
    FaDesktop           = ''
    MdiDesktopClassic   = 'ﲾ'
    MdiDesktopTower     = ''
    CustomWindows       = ''
    FaWindows           = ''
    MdiWindows          = '者'
}
<#$hostSymbol = @{
    Object          = [String]::Format("{0}", "")
    ForegroundColor = $sl.Colors.Host
}#>
$hostSegmentLeft = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.Host
}
$hostSegment = @{
    Object          = [String]::Format(" {0} {1} ", $sl.PromptSymbols.Host.MdiDesktopClassic, $sl.CurrentHostname)
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.Host
}
$hostSegmentRight = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Host
}
#endregion
<##region
    .SECTION DIRECTORY
#>
$sl.PromptSymbols.Dir = @{
    OctFileSubmodule    = ''
    FaFolderOpen        = ''
    FaFolderOpenO       = ''
    MdiFolderOpen       = 'ﱮ'
    MdiFolderMove       = ''
}
<#$dirSymbol = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.Dir.MdiFolderOpen)
}#>
$dirSegmentLeft = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.Dir
}
$dirSegment = @{
    ForegroundColor = $sl.Colors.Black
    BackgroundColor = $sl.Colors.Dir
}
$dirSegmentRight = @{
    Object          = [String]::Format("{0}", $sl.PromptSymbols.PowerlineRightHard)
    ForegroundColor = $sl.Colors.Dir
}
#endregion

<##region
    .SECTION TIME
#>
$sl.PromptSymbols.Time = @{
    FaClockO    = ''
    MdiClock    = ''
    OctClock    = ''
}

#endregion

<##region
    .SECTION PS PROMPT
#>
$sl.PromptSymbols.PS = @{
    MdiConsole      = ''
    MdiConsoleLine  = 'ﲵ'
    FaTerminal      = ''
    OctTerminal     = ''
}
$sl.PromptSymbols.PSSymbol = [String]::Format("{0} ", $sl.PromptSymbols.PS.MdiConsoleLine)
$psPrompt = @{
    Object          = $sl.PromptSymbols.PSSymbol
    ForegroundColor = $sl.Colors.Green
}

$psAdmin = @{
    Object          = $sl.PromptSymbols.PSSymbol
    ForegroundColor = $sl.Colors.Red
}

# inspired by ys theme： not to use special characters (powerline fonts).
# >[https://blog.ysmood.org/my-ys-terminal-theme/]()
