@{
    Name  = 'draculavanhelsing'
    Types = @{
        Directories = @{
            WellKnown = @{
                docs           = 'aa99ff'
                documents      = 'aa99ff'
                desktop        = '364946'
                contacts       = '364946'
                apps           = 'ffaa99'
                applications   = 'ffaa99'
                shortcuts      = 'ffaa99'
                links          = '364946'
                fonts          = 'ff9580'
                images         = 'ffff99'
                photos         = 'ffff99'
                pictures       = 'ffff99'
                videos         = 'ffca80'
                movies         = 'ffca80'
                media          = 'D3D3D3'
                music          = 'ffff99'
                songs          = 'ffff99'
                onedrive       = '99ffee'
                'google drive' = '99ffee'
                downloads      = 'a2ff99'
                repos          = 'aa99ff'
                src            = '00FF7F'
                development    = '00FF7F'
                projects       = '00FF7F'
                bin            = '00FFF7'
                tests          = '708ca9'
                'saved games'  = '414d58'
                '.config'      = '708ca9'
                '.cache'       = '414d58'
                '.vscode'      = '708ca9'
                '.git'         = '708ca9'
                '.github'      = '708ca9'
                'github'       = '708ca9'
                'node_modules' = '708ca9'
                '.vim'         = '708ca9'
                '3d objects'   = '364946'
                'favorites'    = '364946'
                searches       = '364946'
            }
        }
        Files       = @{
            WellKnown = @{
                '.gitattributes'                = '708ca9'
                '.gitignore'                    = '708ca9'
                '.gitmodules'                   = '708ca9'
                '.gitkeep'                      = '708ca9'
                '.gitconfig'                    = '708ca9'
                'git-history'                   = '708ca9'
                'LICENSE'                       = 'CD5C5C'
                'LICENSE.md'                    = 'CD5C5C'
                'LICENSE.txt'                   = 'CD5C5C'
                'CHANGELOG.md'                  = '98FB98'
                'CHANGELOG.txt'                 = '98FB98'
                'CHANGELOG'                     = '98FB98'
                'README.md'                     = 'a2ff99'
                'README.txt'                    = 'a2ff99'
                'README'                        = 'a2ff99'
                '.DS_Store'                     = '696969'
                '.tsbuildinfo'                  = 'F4A460'
                '.jscsrc'                       = 'F4A460'
                '.jshintrc'                     = 'F4A460'
                'tsconfig.json'                 = 'F4A460'
                'tslint.json'                   = 'F4A460'
                'composer.lock'                 = 'F4A460'
                '.jsbeautifyrc'                 = 'F4A460'
                '.esformatter'                  = 'F4A460'
                'cdp.pid'                       = 'F4A460'
                '.htaccess'                     = 'ffff99'
                '.jshintignore'                 = '708ca9'
                '.buildignore'                  = '708ca9'
                '.mrconfig'                     = '708ca9'
                '.yardopts'                     = '708ca9'
                'manifest.mf'                   = '708ca9'
                '.clang-format'                 = '708ca9'
                '.clang-tidy'                   = '708ca9'
                'favicon.ico'                   = 'FFD700'
                '.travis.yml'                   = 'FFE4B5'
                '.gitlab-ci.yml'                = '708ca9'
                '.jenkinsfile'                  = '708ca9'
                'bitbucket-pipelines.yml'       = '708ca9'
                'bitbucket-pipelines.yaml'      = '708ca9'
                '.azure-pipelines.yml'          = 'aa99ff'
                '.config.xlaunch'               = '708ca9'

                # vim files
                '_viminfo'                      = '414d58'
                '_vimrc'                        = '708ca9'

                # nvim files
                '.nvimlog'                      = '414d58'

                # vivaldi
                '.vivaldi_reporting_data'       = '414d58'

                # wsl
                '.wslconfig'                    = '708ca9'

                # Firebase
                'firebase.json'                 = 'ffca80'
                '.firebaserc'                   = 'ffca80'

                # Bower
                '.bowerrc'                      = 'CD5C5C'
                'bower.json'                    = 'CD5C5C'

                # Conduct
                'code_of_conduct.md'            = '8aff80'
                'code_of_conduct.txt'           = 'FFFFE0'

                # Docker
                'Dockerfile'                    = '4682B4'
                'docker-compose.yml'            = '4682B4'
                'docker-compose.yaml'           = '4682B4'
                'docker-compose.dev.yml'        = '4682B4'
                'docker-compose.local.yml'      = '4682B4'
                'docker-compose.ci.yml'         = '4682B4'
                'docker-compose.override.yml'   = '4682B4'
                'docker-compose.staging.yml'    = '4682B4'
                'docker-compose.prod.yml'       = '4682B4'
                'docker-compose.production.yml' = '4682B4'
                'docker-compose.test.yml'       = '4682B4'

                # Vue
                'vue.config.js'                 = '778899'
                'vue.config.ts'                 = '778899'

                # Gulp
                'gulpfile.js'                   = 'CD5C5C'
                'gulpfile.ts'                   = 'CD5C5C'
                'gulpfile.babel.js'             = 'CD5C5C'

                # NodeJS
                'package.json'                  = '6B8E23'
                'package-lock.json'             = '6B8E23'
                '.nvmrc'                        = '6B8E23'
                '.esmrc'                        = '6B8E23'
                '.node_repl_history'            = '708ca9'

                # NPM
                '.nmpignore'                    = '708ca9'
                '.npmrc'                        = '708ca9'

                # Authors
                'authors'                       = 'FF6347'
                'authors.md'                    = 'FF6347'
                'authors.txt'                   = 'FF6347'
            }
            # Archive files
            '.7z'                   = 'DAA520'
            '.bz'                   = 'DAA520'
            '.tar'                  = 'DAA520'
            '.zip'                  = 'DAA520'
            '.gz'                   = 'DAA520'
            '.xz'                   = 'DAA520'
            '.br'                   = 'DAA520'
            '.bzip2'                = 'DAA520'
            '.gzip'                 = 'DAA520'
            '.brotli'               = 'DAA520'
            '.rar'                  = 'DAA520'
            '.tgz'                  = 'DAA520'

            # Executable things
            '.bat'                  = 'ffaa99'
            '.cmd'                  = 'ffaa99'
            '.exe'                  = 'ff9580'
            '.pl'                   = '8A2BE2'

            '.sh'                   = '708ca9'

            # PowerShell
            '.ps1'                  = '80ffea'
            '.psm1'                 = '99ffee'
            '.psd1'                 = '99ffee'
            '.ps1xml'               = '80ffea'
            '.psc1'                 = '80ffea'
            'pssc'                  = '80ffea'

            # Javascript
            '.js'                   = 'F0E68C'
            '.esx'                  = 'F0E68C'
            '.mjs'                  = 'F0E68C'

            # React
            '.jsx'                  = 'ffff80'
            '.tsx'                  = 'ffff80'

            # Typescript
            '.ts'                   = 'F0E68C'

            # Not-executable code files
            '.dll'                  = '708ca9'

            # Importable Data files
            '.clixml'               = 'ff80bf'
            '.csv'                  = 'ff80bf'
            '.tsv'                  = 'ff80bf'

            # Settings
            '.ini'                  = '708ca9'
            '.dlc'                  = '708ca9'
            '.config'               = '708ca9'
            '.conf'                 = '708ca9'
            '.properties'           = '708ca9'
            '.prop'                 = '708ca9'
            '.settings'             = '708ca9'
            '.option'               = '708ca9'
            '.reg'                  = '708ca9'
            '.props'                = '708ca9'
            '.toml'                 = '708ca9'
            '.prefs'                = '708ca9'
            '.sln.dotsettings'      = '708ca9'
            '.sln.dotsettings.user' = '708ca9'
            '.cfg'                  = '708ca9'

            # Source Files
            '.c'                    = 'A9A9A9'
            '.cpp'                  = 'A9A9A9'
            '.go'                   = 'ffff80'
            '.php'                  = '6A5ACD'

            # Visual Studio
            '.csproj'               = 'EE82EE'
            '.ruleset'              = 'EE82EE'
            '.sln'                  = 'EE82EE'
            '.suo'                  = 'EE82EE'
            '.vb'                   = 'EE82EE'
            '.vbs'                  = 'EE82EE'
            '.vcxitems'             = 'EE82EE'
            '.vcxitems.filters'     = 'EE82EE'
            '.vcxproj'              = 'EE82EE'
            '.vsxproj.filters'      = 'EE82EE'

            # CSharp
            '.cs'                   = '7B68EE'
            '.csx'                  = '7B68EE'

            # Haskell
            '.hs'                   = '9932CC'

            # XAML
            '.xaml'                 = '708ca9'

            # Rust
            '.rs'                   = '708ca9'

            # Database
            '.pdb'                  = 'FFD700'
            '.sql'                  = 'FFD700'
            '.pks'                  = 'FFD700'
            '.pkb'                  = 'FFD700'
            '.accdb'                = 'FFD700'
            '.mdb'                  = 'FFD700'
            '.sqlite'               = 'FFD700'
            '.pgsql'                = 'FFD700'
            '.postgres'             = 'FFD700'
            '.psql'                 = 'FFD700'

            # Source Control
            '.patch'                = '708ca9'

            # Project files
            '.user'                 = '414d58'
            '.code-workspace'       = '414d58'

            # Text data files
            '.log'                  = '708ca9'
            '.txt'                  = 'f8f8f2'

            # HTML/css
            '.html'                 = 'CD5C5C'
            '.htm'                  = 'CD5C5C'
            '.xhtml'                = 'CD5C5C'
            '.html_vm'              = 'CD5C5C'
            '.asp'                  = 'CD5C5C'
            '.css'                  = '708ca9'
            '.sass'                 = 'FF00FF'
            '.less'                 = '6B8E23'

            # Markdown
            '.md'                   = '8aff80'
            '.markdown'             = '8aff80'
            '.rst'                  = '8aff80'

            # JSON
            '.json'                 = 'ff80bf'
            '.tsbuildinfo'          = 'ff80bf'

            # YAML
            '.yml'                  = 'FF6347'
            '.yaml'                 = 'FF6347'

            # LUA
            '.lua'                  = '708ca9'

            # Clojure
            '.clj'                  = '00FF7F'
            '.cljs'                 = '00FF7F'
            '.cljc'                 = '00FF7F'

            # Groovy
            '.groovy'               = '708ca9'

            # Vue
            '.vue'                  = 'ffff80'

            # Dart
            '.dart'                 = '4682B4'

            # Elixir
            '.ex'                   = '8B4513'
            '.exs'                  = '8B4513'
            '.eex'                  = '8B4513'
            '.leex'                 = '8B4513'

            # Erlang
            '.erl'                  = 'FF6347'

            # Elm
            '.elm'                  = '9932CC'

            # Applescript
            '.applescript'          = '4682B4'

            # XML
            '.xml'                  = '98FB98'
            '.plist'                = '98FB98'
            '.xsd'                  = '98FB98'
            '.dtd'                  = '98FB98'
            '.xsl'                  = '98FB98'
            '.xslt'                 = '98FB98'
            '.resx'                 = '98FB98'
            '.iml'                  = '98FB98'
            '.xquery'               = '98FB98'
            '.tmLanguage'           = '98FB98'
            '.manifest'             = '98FB98'
            '.project'              = '98FB98'

            # Documents
            '.chm'                  = 'f8f8f2'
            '.pdf'                  = 'f8f8f2'

            # Excel
            '.xls'                  = '8aff80'
            '.xlsx'                 = '8aff80'

            # PowerPoint
            '.pptx'                 = 'ff9580'
            '.ppt'                  = 'ff9580'
            '.pptm'                 = 'ff9580'
            '.potx'                 = 'ff9580'
            '.potm'                 = 'ff9580'
            '.ppsx'                 = 'ff9580'
            '.ppsm'                 = 'ff9580'
            '.pps'                  = 'ff9580'
            '.ppam'                 = 'ff9580'
            '.ppa'                  = 'ff9580'

            # Word
            '.doc'                  = '80ffea'
            '.docx'                 = '80ffea'
            '.rtf'                  = '80ffea'

            # Audio
            '.mp3'                  = 'ffff80'
            '.flac'                 = 'ffff80'
            '.m4a'                  = 'ffff80'
            '.wma'                  = 'ffff80'
            '.aiff'                 = 'ffff80'

            # Images
            '.png'                  = 'ffff80'
            '.jpeg'                 = 'ffff80'
            '.jpg'                  = 'ffff80'
            '.gif'                  = 'ffff80'
            '.ico'                  = 'ffff80'
            '.tif'                  = 'ffff80'
            '.tiff'                 = 'ffff80'
            '.psd'                  = 'ffff80'
            '.psb'                  = 'ffff80'
            '.ami'                  = 'ffff80'
            '.apx'                  = 'ffff80'
            '.bmp'                  = 'ffff80'
            '.bpg'                  = 'ffff80'
            '.brk'                  = 'ffff80'
            '.cur'                  = 'ffff80'
            '.dds'                  = 'ffff80'
            '.dng'                  = 'ffff80'
            '.eps'                  = 'ffff80'
            '.exr'                  = 'ffff80'
            '.fpx'                  = 'ffff80'
            '.gbr'                  = 'ffff80'
            '.img'                  = 'ffff80'
            '.jbig2'                = 'ffff80'
            '.jb2'                  = 'ffff80'
            '.jng'                  = 'ffff80'
            '.jxr'                  = 'ffff80'
            '.pbm'                  = 'ffff80'
            '.pgf'                  = 'ffff80'
            '.pic'                  = 'ffff80'
            '.raw'                  = 'ffff80'
            '.webp'                 = 'ffff80'
            '.svg'                  = 'F4A460'

            # Video
            '.webm'                 = 'ffca80'
            '.mkv'                  = 'ffca80'
            '.flv'                  = 'ffca80'
            '.vob'                  = 'ffca80'
            '.ogv'                  = 'ffca80'
            '.ogg'                  = 'ffca80'
            '.gifv'                 = 'ffca80'
            '.avi'                  = 'ffca80'
            '.mov'                  = 'ffca80'
            '.qt'                   = 'ffca80'
            '.wmv'                  = 'ffca80'
            '.yuv'                  = 'ffca80'
            '.rm'                   = 'ffca80'
            '.rmvb'                 = 'ffca80'
            '.mp4'                  = 'ffca80'
            '.mpg'                  = 'ffca80'
            '.mp2'                  = 'ffca80'
            '.mpeg'                 = 'ffca80'
            '.mpe'                  = 'ffca80'
            '.mpv'                  = 'ffca80'
            '.m2v'                  = 'ffca80'

            # Email
            '.ics'                  = '00CED1'

            # Certifactes
            '.cer'                  = 'FF6347'
            '.cert'                 = 'FF6347'
            '.crt'                  = 'FF6347'
            '.pfx'                  = 'FF6347'

            # Keys
            '.pem'                  = '66CDAA'
            '.pub'                  = '66CDAA'
            '.key'                  = '66CDAA'
            '.asc'                  = '66CDAA'
            '.gpg'                  = '66CDAA'

            # Fonts
            '.woff'                 = 'ff9580'
            '.woff2'                = 'ff9580'
            '.ttf'                  = 'ff9580'
            '.eot'                  = 'ff9580'
            '.suit'                 = 'ff9580'
            '.otf'                  = 'ff9580'
            '.bmap'                 = 'ff9580'
            '.fnt'                  = 'ff9580'
            '.odttf'                = 'ff9580'
            '.ttc'                  = 'ff9580'
            '.font'                 = 'ff9580'
            '.fonts'                = 'ff9580'
            '.sui'                  = 'ff9580'
            '.ntf'                  = 'ff9580'
            '.mrg'                  = 'ff9580'

            # Ruby
            '.rb'                   = 'FF0000'
            '.erb'                  = 'FF0000'
            '.gemfile'              = 'FF0000'

            # FSharp
            '.fs'                   = 'aa99ff'
            '.fsx'                  = 'aa99ff'
            '.fsi'                  = 'aa99ff'
            '.fsproj'               = 'aa99ff'

            # Docker
            '.dockerignore'         = '4682B4'
            '.dockerfile'           = '4682B4'


            # VSCode
            '.vscodeignore'         = '708ca9'
            '.vsixmanifest'         = '708ca9'
            '.vsix'                 = '708ca9'
            '.code-workplace'       = '708ca9'

            # Sublime
            '.sublime-project'      = 'F4A460'
            '.sublime-workspace'    = 'F4A460'

            '.lock'                 = 'DAA520'
        }
    }
}
