#requires -Version 2 -Modules posh-git

function Write-Theme {
    param(
        [bool]
        $lastCommandFailed,
        [string]
        $with
    )
    <#
        .SECTION USER
    #>
    if (Test-NotDefaultUser($sl.CurrentUser)) {
        $prompt += Write-Prompt @userSegment
    }

    $prompt += Write-Prompt -Object " at " -ForegroundColor $sl.Colors.Gray

    <#
        .SECTION HOST
    #>
    $prompt += Write-Prompt @hostSegment

    $prompt += Write-Prompt -Object " in " -ForegroundColor $sl.Colors.Gray

    <#
        .SECTION DIRECTORY
    #>
    $prompt += Write-Prompt @dirSymbol
    $dir    = [String]::Format(" {0} ", (Get-FullPath -dir $pwd))
    $prompt += Write-Prompt @dirSegment -Object $dir

    <#
        .SECTION GIT STATUS
    #>
    $status = Get-VCSStatus
    if ($status) {
        $themeInfo = Get-VcsInfo -status ($status)
        $lastColor = $themeInfo.BackgroundColor
        $prompt += Write-Prompt -Object 'with ' -ForegroundColor $sl.Colors.Gray
        $prompt += Write-Prompt @gitSymbol -ForegroundColor $lastColor
        $prompt += Write-Prompt -Object "$($themeInfo.VcInfo) " -ForegroundColor $lastColor
    }
    <#
        .SECTION TIME
    #>
    $timeStamp = Get-Date -Format "hh:mm tt"
    if (($host.UI.RawUI.CursorPosition.X + $timeStamp.Length) -lt $host.UI.RawUI.WindowSize.Width) {
        $prompt += Set-CursorForRightBlockWrite -textLength ($timeStamp.Length)
        $prompt += Write-Prompt "$timeStamp" -ForegroundColor $sl.Colors.Gray
    }

    # new line
    $prompt += Set-Newline
    # check the last command state and indicate if failed
    if ($lastCommandFailed) {
        $prompt += Write-Prompt @psFailed
    } else {
        if (Test-Administrator) {
            $prompt += Write-Prompt @psAdmin
        } else{
            $prompt += Write-Prompt @psPrompt
        }
    }
}
$sl = $global:ThemeSettings # local settings

<##region
    .SECTION COLORS
#>
$sl.Colors.Black        = [ConsoleColor]::Black
$sl.Colors.Red          = [ConsoleColor]::DarkRed
$sl.Colors.Green        = [ConsoleColor]::DarkGreen
$sl.Colors.Yellow       = [ConsoleColor]::DarkYellow
$sl.Colors.Blue         = [ConsoleColor]::DarkBlue
$sl.Colors.Magenta      = [ConsoleColor]::DarkMagenta
$sl.Colors.Cyan         = [ConsoleColor]::DarkCyan
$sl.Colors.Gray         = [ConsoleColor]::DarkGray
$sl.Colors.BrRed        = [ConsoleColor]::Red
$sl.Colors.BrGreen      = [ConsoleColor]::Green
$sl.Colors.BrYellow     = [ConsoleColor]::Yellow
$sl.Colors.BrBlue       = [ConsoleColor]::Blue
$sl.Colors.BrMagenta    = [ConsoleColor]::Magenta
$sl.Colors.BrCyan       = [ConsoleColor]::Cyan

$sl.Colors.Failed           = $sl.Colors.BrRed
$sl.Colors.User             = $sl.Colors.BrMagenta
$sl.Colors.Host             = $sl.Colors.BrCyan
$sl.Colors.Dir              = $sl.Colors.BrBlue
$sl.Colors.GitDefaultColor  = $sl.Colors.BrGreen
$sl.Colors.WithBG           = $sl.Colors.BrRed
$sl.Colors.WithFG           = $sl.Colors.Black
#endregion

<##region
    .SECTION DEFAULT SYMBOLS
#>
$sl.PromptSymbols.Status = @{
    FaCheck             = ''
    FaCheckSquare       = ''
    FaCheckSquareO      = ''
    FaEyeSlash          = ''
    FaExclamation       = ''
    FaLevelDown         = ''
    FaLevelUp           = ''
    FaQuestion          = ''
    FaWarning           = ''
    MdiAlertO           = ''
    MdiCanceled         = 'ﰸ'
    MdiCommentAlert     = ''
    MdiCommentAlertO    = ''
    MdiCommentCheck     = ''
    MdiCommentCheckO    = ''
    MdiCommentProcess   = ''
    MdiCommentProcessO  = ''
    MdiCloudCheck       = ''
    MdiCloudDownload    = ''
    MdiCloudSync        = 'מּ'
    MdiCloudUpload      = ''
    MdiEmoticonDead     = 'ﮙ'
    MdiEyeOff           = ''
    MdiEyeOffO          = '﯏'
    OctCloudDownload    = ''
    OctCloudUpload      = ''
    OctSync             = ''
    OctThumbsUp         = ''
    OctThumbsDown       = ''
}
$sl.PromptSymbols.FailedCommandSymbol   = [String]::Format("{0} ", $sl.PromptSymbols.Status.MdiEmoticonDead)
$psFailed = @{
    Object          = [String]::Format("{0} ", $sl.PromptSymbols.Status.MdiEmoticonDead)
    ForegroundColor = $sl.Colors.Failed
}

$sl.PromptSymbols.HomeSymbol = '~'
$sl.PromptSymbols.PathSeparator = '\'
$sl.PromptSymbols.Root = @{
    FaHdd           = ''
    MdiDesktop      = 'ﲾ'
    MdiDesktopTower = ''
    FaGlobe         = ''
}
$sl.PromptSymbols.RootSymbol    = [String]::Format(" {0} ", $sl.PromptSymbols.Root.MdiDesktopTower)

$sl.PromptSymbols.Ellipsis = @{
    FaEllipsis    = ''
    OctEllipsis   = ''
    MdiDots       = ''
}
$sl.PromptSymbols.TruncatedFolderSymbol = [String]::Format("{0}", $sl.PromptSymbols.Ellipsis.MdiDots)
<#
    .SECTION GIT SYMBOLS
#>
$sl.PromptSymbols.Git = @{
    DevGit          = ''
    FaGit           = ''
    FaGitSquare     = ''
    DevGitBranch    = ''
    DevGitCommit    = ''
    MdiGitBranch    = 'שׂ'
    OctGitBranch    = ''
    OctGitCommit    = ''
    FaCode          = ''
    FaCodeFile      = ''
    MdiCode         = ''
    MdiCodeCheck    = 'ﮒ'
    MdiCodeBraces   = ''
    OctCode         = ''
}
$sl.GitSymbols.BranchSymbol                     = [String]::Format("{0} ", $sl.PromptSymbols.Git.FaGit)
$sl.GitSymbols.BranchBehindStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaLevelDown)
$sl.GitSymbols.BranchAheadStatusSymbol          = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaLevelUp)
$sl.GitSymbols.BranchUntrackedSymbol            = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaEyeSlash)
$sl.GitSymbols.BranchIdenticalStatusToSymbol    = [String]::Format("{0} ", $sl.PromptSymbols.Status.OctSync)
$sl.GitSymbols.LocalDefaultStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaCheck)
$sl.GitSymbols.LocalWorkingStatusSymbol         = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaQuestion)
$sl.GitSymbols.LocalStagedStatusSymbol          = [String]::Format("{0} ", $sl.PromptSymbols.Status.FaExclamation)
$sl.GitSymbols.BeforeWorkingSymbol              = [String]::Format("{0} ", $sl.PromptSymbols.Git.MdiCode)
$sl.GitSymbols.BeforeIndexSymbol                = [String]::Format("{0} ", $sl.PromptSymbols.Git.MdiCodeCheck)

<## region
    .SECTION USER
#>
$sl.PromptSymbols.User = @{
    FaUser            = ''
    FaUserO           = ''
    FaUserCircle      = ''
    FaUserCircleO     = ''
    FaIdCard          = ''
    FaAddressCard     = ''
    MdiAccountCircle  = ''
    MdiAccountBox     = ''
    MdiAccountCard    = '𣏕'
}
$userSegment = @{
    Object          = [String]::Format("{0}  {1}", $sl.PromptSymbols.User.FaUserCircle, $sl.CurrentUser)
    ForegroundColor = $sl.Colors.User
}
# endregion
<##region
    .SECTION HOST
#>
$sl.PromptSymbols.Host = @{
    FaDesktop           = ''
    MdiDesktopClassic   = 'ﲾ'
    MdiDesktopTower     = ''
    CustomWindows       = ''
    FaWindows           = ''
    MdiWindows          = '者'
}
$hostSegment = @{
    Object          = [String]::Format("{0}  {1}", $sl.PromptSymbols.Host.FaWindows, $sl.CurrentHostname)
    ForegroundColor = $sl.Colors.Host
}
#endregion
<##region
    .SECTION DIRECTORY
#>
$sl.PromptSymbols.Dir = @{
    OctFileSubmodule    = ''
    FaFolderOpen        = ''
    FaFolderOpenO       = ''
    MdiFolderOpen       = 'ﱮ'
    MdiFolderMove       = ''
}
$dirSymbol  = @{
    Object          = [String]::Format("{0} ", $sl.PromptSymbols.Dir.FaFolderOpen)
    ForegroundColor = $sl.Colors.Dir
}
$dirSegment = @{
    ForegroundColor = $sl.Colors.Dir
}
#endregion

<##region
    .SECTION TIME
#>
$sl.PromptSymbols.Time = @{
    FaClockO    = ''
    MdiClock    = ''
    OctClock    = ''
}

#endregion

<##region
    .SECTION PS PROMPT
#>
$sl.PromptSymbols.PS = @{
    MdiConsole      = ''
    MdiConsoleLine  = 'ﲵ'
    FaTerminal      = ''
    OctTerminal     = ''
}
$sl.PromptSymbols.PSSymbol = [String]::Format("{0} ", $sl.PromptSymbols.PS.MdiConsoleLine)
$psPrompt = @{
    Object          = $sl.PromptSymbols.PSSymbol
    ForegroundColor = $sl.Colors.Green
}

$psAdmin = @{
    Object          = $sl.PromptSymbols.PSSymbol
    ForegroundColor = $sl.Colors.Red
}

# inspired by ys theme： not to use special characters (powerline fonts).
# >[https://blog.ysmood.org/my-ys-terminal-theme/]()
