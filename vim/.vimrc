" =============================================================================
" ~/.vimrc
"
" The user's vimrc file contains optional runtime configurations settings to
" initialize Vim each time it starts.
"
" You can customize Vim by putting suitable commands in your ~/.vimrc
" =============================================================================

" Enable syntax highlighting
syntax on
" Enable filetype plugins and syntax
filetype plugin on
" Enable intelligent auto-identing based on filetype
filetype indent on
" Enable use of mouse
set mouse=a

" Replace tabs with spaces
set expandtab
" Be smart when using tabs :)
set smarttab
" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2
" Enable autoindent on next line
set autoindent
set smartindent

" Always display the status line
set laststatus=2

" Enable line numbers
set number
" Set 7 lines to the cursor - when moving vertically up/down a file
set so=7
" Always show current position
set ruler
" Height of command bar
set cmdheight=2

" show matching brackets when cursor is over them
set showmatch
" set blink rate for matching brackets
set mat=2
" Configure backspace to act as it should
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
