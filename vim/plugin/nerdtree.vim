let g:NERDTreeWinPos = 'left'
"let g:NERDTreeDirArrowExpandable = ''
"let g:NERDTreeDirArrowCollapsible = ''
let g:NERDTreeShowHidden=1
let g:NERDTreeWinSize=35
" Ctrl + A to open NERD Tree
map <C-a> :NERDTreeToggle<CR>
" autostart on no file
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" close tree when all files close
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

