set encoding=utf-8
scriptencoding utf-8
"let g:airline_theme='base16'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.crypt = ' '
let g:airline_symbols.linenr = '  '
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = ' '
let g:airline_symbols.branch = ' '
let g:airline_symbols.paste = ' '
let g:airline_symbols.paste = ' '
let g:airline_symbols.paste = ' '
let g:airline_symbols.spell = '暈 '
let g:airline_symbols.notexists ='ﬤ  '
let g:airline_symbols.whitespace = 'Ξ'

let g:airline_exclude_preview=1

let g:airline#extensions#coc#enabled=1
let g:airline#extensions#coc#error_symbol=' 𥉉 '
let g:airline#extensions#coc#warning_symbol='   '

let g:airline#extensions#branch#enabled=1

let g:airline#extensions#cursormode#enabled=1


let g:airline#extensions#denite#enabled=1

let g:airline#extensions#fzf#enabled=1

let g:airline#extensions#neomake#enabled=1
let g:airline#extensions#neomake#error_symbol=' 𥉉 '
let g:airline#extensions#neomake#warning_symbol='   '

let g:airline#extensions#virtualenv#enabled=1

let g:airline#extensions#capslock#enabled=1
let g:airline#extensions#capslock#symbol='בּ '
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#whitespace#enabled=1
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#formatter = 'unique_tail'
