" ==============================================================================
" NeoVim configuration file
"
" @author Khang M. Nguyen
" ==============================================================================
"
" ******************************************************************************
"  Plugins Install
" ******************************************************************************
let g:plug_shallow = '0'
" plugin directory
call plug#begin('~/.local/share/nvim/plugged')

"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

" A Git Wrapper so awesome, it should be illegal
Plug 'tpope/vim-fugitive'

" quoting/parenthesizing made simple
"Plug 'tpope/vim-surround'

" A tree explorer plugin for vim
Plug 'scrooloose/nerdtree'

" Vim plugin for intensely nerdy commenting powers
Plug 'scrooloose/nerdcommenter'

" A plugin of NERDTree showing git status
Plug 'xuyuanp/nerdtree-git-plugin'

" command-line fuzzy finder
Plug 'junegunn/fzf.vim', { 'do': { -> fzf#install() } }

" Display tags in window, ordered by scope
Plug 'majutsushi/tagbar'

" Perform all your vim insert mode completions with Tab
"Plug 'ervandew/supertab'

" Vim motions on speed
Plug 'easymotion/vim-easymotion'

" True Sublime Text style multiple selections for Vim
Plug 'mg979/vim-visual-multi', { 'branch': 'master' }

" Airline Status Bars
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" A Vim plugin which shows git diff markers in the sign column
Plug 'airblade/vim-gitgutter'

" indent line
Plug 'yggdroot/indentLine'

" EditorConfig plugin for Vim
"Plug 'editorconfig/editorconfig'

" A git mirror of gundo.vim
Plug 'sjl/gundo.vim'

" Highlights trailing whitespace in red
Plug 'bronson/vim-trailing-whitespace'

" A solid language pack for Vim
Plug 'sheerun/vim-polyglot'

" interpret a file by function and cache file automatically
Plug 'marcweber/vim-addon-mw-utils'

" utility functions
"Plug 'tomtom/tlib_vim'

"  base16 vim
Plug 'chriskempson/base16-vim'

" Bring physics-based smooth scrolling to the Vim world
Plug 'yuttie/comfortable-motion.vim'

" Vim plugin, insert or delete brackets, parens, quotes in pair
Plug 'jiangmiao/auto-pairs'

" devicons
Plug 'ryanoasis/vim-devicons'

" intelligently reopen files at last edit position
Plug 'farmergreg/vim-lastplace'
"
" vim + tmux
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'benmills/vimux'
Plug 'christoomey/vim-tmux-navigator'
"
Plug 'sbdchd/neoformat'
" Intellisense engine
Plug 'neoclide/coc.nvim', { 'branch': 'release' }

" A Vim plugin that highlights the enclosing html/xml tags
Plug 'alvan/vim-closetag'

Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'shougo/neoyank.vim'
" MRU plugin includes unite.vim/denite.nvim MRU sources
Plug 'shougo/neomru.vim'

Plug 'neomake/neomake'

Plug 'pprovost/vim-ps1'

" initialize plugin system
call plug#end()
