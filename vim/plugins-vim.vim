" ==============================================================================
" NeoVim configuration file
"
" @author Khang M. Nguyen
" ==============================================================================
"
" ******************************************************************************
"  Plugins Install
" ******************************************************************************
let g:plug_shallow = '0'
" plugin directory
call plug#begin('~/vimfiles/plugged')

" A tree explorer plugin for vim
Plug 'scrooloose/nerdtree'
" Vim plugin for intensely nerdy commenting powers
Plug 'scrooloose/nerdcommenter'
" A plugin of NERDTree showing git status
Plug 'xuyuanp/nerdtree-git-plugin'

" Airline Status Bars
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Check syntax in Vim asynchronously and fix files, with Language Server Protocol support
Plug 'w0rp/ale'
" indent line
Plug 'yggdroot/indentLine'
" Vim plugin, provides insert mode auto-completion for quotes, parens, brackets, etc
Plug 'raimondi/delimitmate'

" A solid language pack for Vim
Plug 'sheerun/vim-polyglot'

" interpret a file by function and cache file automatically
Plug 'marcweber/vim-addon-mw-utils'

"  base16 vim
Plug 'chriskempson/base16-vim'
" devicons
Plug 'ryanoasis/vim-devicons'

" intelligently reopen files at last edit position
Plug 'farmergreg/vim-lastplace'

" Better whitespace highlighting for Vim
Plug 'ntpeters/vim-better-whitespace'

"Tagbar is a Vim plugin that provides an easy way to browse the tags of the
"current file and get an overview of its structure.
Plug 'majutsushi/tagbar'

" Provide easy code formatting in Vim by integrating existing code formatters
Plug 'chiel92/vim-autoformat'

" Configure tabs within Terminal Vim
Plug 'mkitt/tabline.vim'

" buffer status
Plug 'bling/vim-bufferline'

Plug 'wincent/terminus'

" Lint Vim files
Plug 'dbakker/vim-lint'

" initialize plugin system
call plug#end()

