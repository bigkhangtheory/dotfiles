#!/bin/sh
LEFT=$HOME/.wallpaper/bg_left
MIDDLE=$HOME/.wallpaper/bg_middle
RIGHT=$HOME/.wallpaper/bg_right
SPAN=$HOME/.wallpaper/bg_span

feh --bg-scale $LEFT --bg-scale $MIDDLE --bg-scale $RIGHT
#feh --bg-scale --no-xinerama $SPAN
